package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        Stream<String> songName_Ordered = songs.stream().map(s -> s.getSongName()).sorted();
        return songName_Ordered;
    }

    @Override
    public Stream<String> albumNames() {
        Stream<String> ris_albumNames = albums.keySet().stream();
        return ris_albumNames;
    }

    @Override
    public Stream<String> albumInYear(final int year) {

        final Set<String> nameAlbumInYear = new HashSet<>();
        albums.forEach((k, v) -> {
            if (v == year) {
                nameAlbumInYear.add(k);
            }
        });

        Stream<String> ris_nameAlbumInYear = nameAlbumInYear.stream();
        return ris_nameAlbumInYear;
    }

    @Override
    public int countSongs(final String albumName) {
        int counter = songs.stream().filter(s -> s.getAlbumName().isPresent())
                .filter(s -> s.getAlbumName().get().equals(albumName)).mapToInt(s -> 1).sum();
        return counter;
    }

    @Override
    public int countSongsInNoAlbum() {
        int counter = songs.stream().filter(s -> !s.getAlbumName().isPresent()).mapToInt(s -> 1).sum();
        return counter;
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {

        OptionalDouble res = songs.stream().filter(s -> s.getAlbumName().isPresent())
                .filter(s -> s.getAlbumName().get().equals(albumName)).mapToDouble(p -> p.getDuration()).average();
        return res;
    }

    @Override
    public Optional<String> longestSong() {

        Optional<String> result = songs.stream().sorted((s1, s2) -> Double.compare(s2.getDuration(), s1.getDuration()))
                .findFirst().map(s -> s.getSongName());
        return result;
    }

    @Override
    public Optional<String> longestAlbum() {
        final Map<Optional<String>, Double> album_duration = new HashMap<>();

        for (Song song : songs) {
            if (album_duration.containsKey(song.getAlbumName())) {
                double totDur = album_duration.get(song.getAlbumName());
                totDur += song.getDuration();
                album_duration.replace(song.getAlbumName(), totDur);
            } else {
                album_duration.put(song.getAlbumName(), song.getDuration());
            }
        }

        Optional<String> max = album_duration.entrySet().stream()
                .max((a1, a2) -> a1.getValue() > a2.getValue() ? 1 : -1).get().getKey();
        return max;
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
